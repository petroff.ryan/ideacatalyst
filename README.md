# ideacatalyst
My own web implementation of Approximately The Thing From The Future. Now working! npm install -g imagineer

If you just said out loud "There's no way I'm installing this globally on my dev machine!", you'll find it's also demonstrated on jsfiddle https://jsfiddle.net/kaqustub/17/
You'll also find an additional frontend to imagineer hosted at https://chilling.today/  --  Give it a try!  

This 'imagineer' will supply a random Subject, Object, Era, and Mood, and then the user generates an idea that fits. Mash ideas together to create a better world!
Sample output:
```
{ object: 'Clothing',
  subject: 'Randomness',
  mood: 'Quixotic',
  era: 'Next Year' }
```
```
{ object: 'Accessory',
  subject: 'Space/Time',
  mood: 'Energetic',
  era: 'Next Gigasecond (31.558 Years)' }
```


Thanks go out to https://situationlab.org/project/the-thing-from-the-future/ for the inspiration for this project!


Creative Commons License
Imagineer by Ryan Petroff is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
